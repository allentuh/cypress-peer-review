/// <reference types="cypress" />

const testServer = 'local';
const sd001Email = 'allen+sd001@incube8.sg';
const dob = '11/11/1999';
const username = 'SD001';
const imgPath = 'common/sd001.jpg';
const location = 'Las Vegas, NV, USA';
const sd001Header = 'SD001 Test Account';
const timeoutMedium = 20000;

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Actions', () => {
    beforeEach(() => {});

    it('Create sugar daddy account', () => {
        // Create login credential
        cy.visit(`https://${testServer}.seeking.com/join`);
        cy.get('[data-value="3"]').click();
        cy.get('[data-value="249"]').click();
        cy.get('[data-value="1"]').click();
        cy.get('input[name="email"]').type(sd001Email);
        cy.get('input[name="dob"]').type(dob);
        cy.get('[data-cy-button="submit"]').click();
        cy.url({ timeout: timeoutMedium }).should('eq', `https://${testServer}.seeking.com/member?registered=true`);

        // Complete the IPCF
        cy.get('[data-cy-input="username"]', { timeout: timeoutMedium }).focus().type(username).blur();
        cy.get('[data-cy-message="success"]', { timeout: timeoutMedium }).should('be.visible');
        cy.get('[data-cy-input="file"]').attachFile(imgPath);
        cy.get('[data-cy-button="submit"]', { timeout: timeoutMedium }).click();
        cy.get('.profile-completion-next-step-2', { timeout: timeoutMedium }).should('be.visible').click();

        cy.get('input[placeholder="Type city here"]', { timeout: timeoutMedium }).should('be.visible').type(location);
        cy.contains(location).click();
        cy.get('[data-cy-next-step="3"]').click();

        cy.get('[data-cy-select="height"]', { timeout: timeoutMedium }).should('be.visible').select(`6'0"`);
        cy.contains('Athletic').click();
        cy.contains('Asian').click();
        cy.get('[data-cy-next-step="4"]').click();

        cy.get('[data-education-value=5]', { timeout: timeoutMedium }).should('be.visible').click();
        cy.get('[data-relationship_status-value=14]').click();
        cy.get('.initial-children-no').click();
        cy.contains('Non Smoker').click();
        cy.contains('Non Drinker').click();
        cy.get('[data-cy-next-step="5"]').click();

        cy.get('[data-cy-select="net_worth"]', { timeout: timeoutMedium }).should('be.visible').select('$100000');
        cy.get('[data-cy-select="income"]').select('$1000000');
        cy.get('[data-cy-next-step="6"]').click();

        cy.contains('Active lifestyle', { timeout: timeoutMedium }).should('be.visible').click();
        cy.get('.Continue_div > .profile-completion-next-step-6').click();

        cy.get('[data-cy-input="heading"]', { timeout: timeoutMedium }).should('be.visible').type(sd001Header);
        cy.get('[data-cy-textarea="about_me"]').type('This is about me textarea i need to write about myself');
        cy.get('[data-cy-next-step="finish"]').click();
    });

    it('Activate sugar daddy account', () => {
        cy.request('GET', `https://api-${testServer}.seeking.com/v3/profile/get-profile-uuid/${sd001Email}`)
            .then((response) => {
                const uid = response.body.response.uid;

                cy.request('GET', `https://api-${testServer}.seeking.com/v3/users/${uid}/force-activate-account?password=asdasd`)
                    .then((response) => {
                        expect(response.status).to.eq(200);
                    });
            });
    });

    it('Approve sugar daddy account', () => {
        cy.request('GET', `https://api-${testServer}.seeking.com/v3/profile/get-profile-uuid/${sd001Email}`)
            .then((response) => {
                const uid = response.body.response.uid;

                cy.request('GET', `https://api-${testServer}.seeking.com/v3/users/${uid}/force-approve-profile`)
                    .then((response) => {
                        expect(response.status).to.eq(200);
                    });
            });
    });

    it('Upgrade sugar daddy account to Diamond subscription', () => {
        cy.visit(`https://${testServer}.seeking.com/billing/memberships?ref=upgrade`);
        cy.get('input[name = "email"]').type(sd001Email);
        cy.get('input[name = "password"]').type('asdasd');
        cy.get('[data-cy-button="submit"]').click();
        cy.get('.onSideMessageClose', { timeout: 10000 }).click();
        cy.get('.DiamondPackageItem-cover', { timeout: 30000 }).click();
        cy.get('input[name = "fullName"]').type('John Smith');
        cy.get('input[name = "billingAddress1"]').type('Hollywood');
        cy.get('select[name = "billingCountry"]').select('United States');
        cy.get('input[name = "billingCity"]').type('New York');
        cy.get('input[name = "billingState"]').type('CA');
        cy.get('input[name = "billingPostCode"]').type('94608');
        cy.get('input[name = "billingPhone"]').type('+16588887777');
        cy.get('input[name = "number"]').type('4111111111111111');
        const year = (new Date()).getFullYear() -2000 + 2;
        cy.get('input[name = "expiryDate"]').type(`01/${year}`);
        cy.get('input[name = "cvv"]').type('123');
        cy.get('[data-cy-button="confirm-bg-check-payment"]').click();
    });
});