/// <reference types="cypress" />

const testServer = "test1";

Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

context("Peer review for SATHREE-27731 for public pages in web view", () => {
  beforeEach(() => {});

  it("see all the links are not broken", () => {
    cy.fixture("public_pages").then((links) => {
      links.forEach((link) => {
        const fullUrl =
          `https://${testServer}.seeking.com/` +
          link +
          "?ref=test&ref1=test1&ref2=test2";

        let withParam = true;

        if (
          link === "browse/members/sugar-baby-female" ||
          link === "browse/members/sugar-baby" ||
          link === "what-is-a-sugar-baby"
        ) {
          link = "/sugar-baby";
        } else if (
          link === "browse/members/sugar-daddy" ||
          link === "browse/members/sugar-mommy" ||
          link === "what-is-a-sugar-daddy"
        ) {
          link = "/sugar-daddy";
        } else if (link.indexOf("calendar-2016/") >= 0) {
          link = "/calendar-2016";
        } else if (link.indexOf("calendar-2017/") >= 0) {
          link = "/calendar-2017";
        } else if (link.indexOf("calendar-2018/") >= 0) {
          link = "/calendar-2018";
        } else if (link.indexOf("calendar-2019/") >= 0) {
          link = "/calendar";
        } else if (link.indexOf("glossary/sugar-dating/allowance") >= 0) {
          link = "/glossary/sugar-dating";
        } else if (link === "gay-pride-2017") {
          link = "/joinfree?LP=MM";
          withParam = false;
        } else if (
          link === "seekingmillionaire" ||
          link === "sugar-dating-heat-map"
        ) {
          link = "";
        } else if (link != "") {
          link = "/" + link;
        }

        const enFullUrl =
          `https://${testServer}.seeking.com/en` +
          link +
          (withParam ? "?ref=test&ref1=test1&ref2=test2" : "");

        cy.visit(fullUrl);

        cy.get(`a[href*="${enFullUrl}"`, { timeout: 20000 });
      });
    });
  });
});
