/// <reference types="cypress" />

const testServer = 'test1';

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-27529 for public pages in web view', () => {
    beforeEach(() => {})

    it('see all the server 500 redirect', () => {
        cy.fixture('sathree-27529').then((links) => {
            links.forEach(link => {
                const fullUrl = `https://${testServer}.seeking.com/` + link ;

                cy.visit(fullUrl).screenshot(link.replace(/\//g, '-'), {
                    capture: 'runner'
                });
            });
        })
    })
})