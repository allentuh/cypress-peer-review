/// <reference types="cypress" />

const testServer = "test1";

Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

context("Peer review for SATHREE-28650 for summit pages canonical in web view", () => {
  beforeEach(() => {});

  it("see all the pages has canonical data", () => {
    cy.fixture("sa_summit_pages").then((links) => {
      links.forEach((link) => {
        const fullUrl = `https://${testServer}.seeking.com/` + link;
        let canonicalUrl = fullUrl;

        const summits = [
          "summit-2016",
          "summit-2017",
          "summit-2018",
          "sugar-baby-summit-speaker-auditions"
        ];

        for (const element of summits) {
          if (fullUrl.includes(element)) {
            if (element == "sugar-baby-summit-speaker-auditions") {
              canonicalUrl = canonicalUrl.replace(element, "summit");
            } else {
              canonicalUrl = canonicalUrl.substr(0, canonicalUrl.indexOf(element) + element.length);
            }
            break;
          }
        }

        cy.visit(fullUrl);
        cy.get(`link[rel="canonical"]`, { timeout: 20000 })
          .should("have.attr", "href")
          .and("equal", canonicalUrl);
      });
    });
  });
});
