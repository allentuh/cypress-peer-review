/// <reference types="cypress" />

context('Actions', () => {
    beforeEach(() => {});

    it('Manual login to test server with Google account', () => {
        cy.visit(`https://dashboard.pusher.com/apps/704601/errors/webhook`);
        cy.contains('Sign in with Google').click();
        cy.url().should('eq', `https://dashboard.pusher.com/apps/704601/errors/webhook`);

        cy.get('#events-list')
            .get('tr>td')
            .each(($el, index, $list) => {
                let data = '';

                if ($el.find('time').length > 0) {
                    data = $el.find('time').attr('datetime') + '|';
                } else if ($el.find('code').length > 0) {
                    const jsonObject = JSON.parse($el.find('code').text());
                    data = jsonObject.events[0].channel.replace('presence-videocall.', '') + '|';
                    data += jsonObject.events[0].user_id + '|';
                    data +=
                        $el
                            .find('code')
                            .text()
                            .replace(/(\r\n|\n|\r| )/gm, '')
                            .trim() + '\n';
                } else {
                    data = $el.html().trim() + '|';
                }

                const date = new Date();
                const month = ('0' + (date.getMonth() + 1)).slice(-2);
                const day = ('0' + date.getDate()).slice(-2);
                cy.writeFile(`cypress/logs/pusher_webhook_error_${date.getFullYear()}${month}${day}.csv`, data, {
                    flag: 'a+',
                    encoding: 'ascii',
                });
            });
    });
});
