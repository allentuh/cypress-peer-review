/// <reference types="cypress" />

const testServer = 'test1';
const addContext = require('mochawesome/addContext')

Cypress.on('test:after:run', (test, runnable) => {
    if (test.state === 'failed') {
        const screenshotFileName = `${runnable.parent.title} -- ${test.title} (failed).png`
        addContext({ test }, `assets/${Cypress.spec.name}/${screenshotFileName}`)
    }
})

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-26903 for calendar pages locale link in web view', () => {
    beforeEach(() => {})

    it('see all the pages redirect correctly', () => {
        cy.fixture('locale').then((locale) => {
            locale.forEach(locale => {
                let url = `https://${testServer}.seeking.com/${locale}/calendar-2016/akira`;
                cy.visit(url)
                cy.url().should('eq', `https://${testServer}.seeking.com/${locale}/calendar-2016`)

                url = `https://${testServer}.seeking.com/${locale}/calendar-2017/giselle`;
                cy.visit(url)
                cy.url().should('eq', `https://${testServer}.seeking.com/${locale}/calendar-2017`)

                url = `https://${testServer}.seeking.com/${locale}/calendar-2019/carley`;
                cy.visit(url)
                cy.url().should('eq', `https://${testServer}.seeking.com/${locale}/calendar`)
            });
        })
    })
})