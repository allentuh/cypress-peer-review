/// <reference types="cypress" />

const testServer = 'test1';
const addContext = require('mochawesome/addContext')

Cypress.on('test:after:run', (test, runnable) => {
    if (test.state === 'failed') {
        const screenshotFileName = `${runnable.parent.title} -- ${test.title} (failed).png`
        addContext({ test }, `assets/${Cypress.spec.name}/${screenshotFileName}`)
    }
})

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-26903 for calendar pages in web view', () => {
    beforeEach(() => {})

    it('see all the pages redirect correctly', () => {
        cy.fixture('calendar_pages').then((links) => {
            links.forEach(link => {
                const url = `https://${testServer}.seeking.com/` + link;

                cy.visit(url)

                const index = link.indexOf('/')
                let sub = link
                if (index > 0) {
                    sub = link.substring(0, index)

                    if (sub === 'calendar-2019') {
                        sub = 'calendar'
                    }
                } else {
                    if (sub === 'china-unavailability') {
                        sub = 'zh'
                    } else if (sub === 'china-unavailability-temporary') {
                        sub = 'zh'
                    } else if (sub === 'gay-pride-2017') {
                        sub = 'joinfree?LP=MM'
                    }
                }

                cy.url().should('eq', `https://${testServer}.seeking.com/` + sub)
            });
        })
    })
})