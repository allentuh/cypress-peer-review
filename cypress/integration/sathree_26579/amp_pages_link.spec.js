/// <reference types="cypress" />

const testServer = 'local';

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-2579 AMP pages', () => {
    beforeEach(() => {})

    it('see all the footer element in AMP page', () => {
        cy.fixture('amp_pages').then((links) => {
            links.forEach(link => {
                cy.visit(`https://${testServer}.seeking.com/` + link)
            });
        })
    })
})