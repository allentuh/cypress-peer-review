/// <reference types="cypress" />

const testServer = 'test5';

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-2579 for public pages in mobile view', () => {
    beforeEach(() => {})

    it('see all the footer element in page', () => {
        cy.viewport('iphone-6');

        cy.fixture('public_pages').then((links) => {
            links.forEach(link => {
                cy.visit(`https://${testServer}.seeking.com/` + link)
                cy.get('#App-footer')
                    .toMatchImageSnapshot({
                        "name": "base-mobile-footer",
                    })
            });
        })
    })
})