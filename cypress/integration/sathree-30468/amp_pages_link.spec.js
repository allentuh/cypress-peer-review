/// <reference types="cypress" />

const testServer = "test5";

Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

context("Peer review for SATHREE-30468 for cookie disclaimer in web view", () => {
  beforeEach(() => {});

  it("see all the links are not broken", () => {
    cy.fixture("amp_pages").then((links) => {
      links.forEach((link) => {
        cy.visit(`https://${testServer}.seeking.com/` + link);

        cy.get('.disclaimerConsentText', { timeout: 20000 }).should('have.html', '\n      Seeking.com uses cookies, which are necessary for this site to work properly.\n      <a class="disclaimerConsentLink" rel="noreferrer no opener no referer" target="_blank" href="https://test5.seeking.com/privacy">\n        Learn more\n      </a>\n    ');
      });
    });
  });
});
