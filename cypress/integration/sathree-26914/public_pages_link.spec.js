/// <reference types="cypress" />

const testServer = 'local';

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

context('Peer review for SATHREE-26914 for public pages in web view', () => {
    beforeEach(() => {})

    it('see all the query string remain in footer language links', () => {
        cy.fixture('public_pages').then((links) => {
            links.forEach(link => {
                const fullUrl = `https://${testServer}.seeking.com/` + link + '?ref=test&ref1=test1&ref2=test2';
                const enFullUrl = `https://${testServer}.seeking.com/en/` + link + '?ref=test&ref1=test1&ref2=test2';
                const esFullUrl = `https://${testServer}.seeking.com/es/` + link + '?ref=test&ref1=test1&ref2=test2';
                const frFullUrl = `https://${testServer}.seeking.com/fr/` + link + '?ref=test&ref1=test1&ref2=test2';
                const deFullUrl = `https://${testServer}.seeking.com/de/` + link + '?ref=test&ref1=test1&ref2=test2';
                const zhFullUrl = `https://${testServer}.seeking.com/zh/` + link + '?ref=test&ref1=test1&ref2=test2';
                const nlFullUrl = `https://${testServer}.seeking.com/nl/` + link + '?ref=test&ref1=test1&ref2=test2';
                const ptFullUrl = `https://${testServer}.seeking.com/pt/` + link + '?ref=test&ref1=test1&ref2=test2';
                const jaFullUrl = `https://${testServer}.seeking.com/ja/` + link + '?ref=test&ref1=test1&ref2=test2';

                cy.visit(fullUrl)

                cy.get(`a[href*="${enFullUrl}"`);
                cy.get(`a[href*="${esFullUrl}"`);
                cy.get(`a[href*="${frFullUrl}"`);
                cy.get(`a[href*="${deFullUrl}"`);
                cy.get(`a[href*="${zhFullUrl}"`);
                cy.get(`a[href*="${nlFullUrl}"`);
                cy.get(`a[href*="${ptFullUrl}"`);
                cy.get(`a[href*="${jaFullUrl}"`);
            });
        })
    })
})